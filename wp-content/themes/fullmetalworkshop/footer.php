</div>
<div id="toggle-menu">
	<div class="toggle-line"></div>
</div>
<div id="menu-overlay">
	<nav class="main-navigation" role="navigation">
		<?php wp_nav_menu( array('menu' => 'header-menu') );?>
	</nav>
</div>
<div class="mail-icon">
	<a href="mailto:yourfriends@fullmetalworkshop.com">
		<?php
		$mailIcon = is_front_page() && is_page() ? get_template_directory_uri().'/images/mail.png' : get_template_directory_uri().'/images/blackmail.png'; ?>
		<img src="<?php echo $mailIcon; ?>"/>
	</a>
</div>
<!-- <?php
$image = is_front_page() && is_page() ? 'fmw_logo_big_2.png' : 'fmw_logo_big_1.png'; ?>
<div class="logo-text">
	<img src="<?php echo get_template_directory_uri(); ?>/images/<?php echo $image; ?>"/>
</div> -->
<?php
$image = is_front_page() && is_page() ? 'logo.png' : 'logo_black.png'; ?>
<div class="logo">
	<a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_html( get_bloginfo( 'name' ) ); ?>" rel="home">
		<img src="<?php echo get_template_directory_uri(); ?>/images/<?php echo $image; ?>"/>
	</a>
</div>
<div id="mc-embed-signup">
	<h4>Get the Color Correspondence newsletter!</h4>
	<form action="https://fullmetalworkshop.us17.list-manage.com/subscribe/post?u=8b032d8566f5bc3f3417f02c3&amp;id=ef28567184" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
	    <div id="mc-embed-signup-scroll">

		<input type="email" value="" name="EMAIL" class="email" id="mce-EMAIL" placeholder="email address" required>
	    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
	    <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_8b032d8566f5bc3f3417f02c3_ef28567184" tabindex="-1" value=""></div>
	    <div class="clear"><input type="submit" value="SIGN UP" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
	    </div>
	</form>
</div>
<footer id="footer" role="contentinfo">
	<?php if( is_front_page() && is_page() ){ ?>
	<div class="scroll-down">
		<p>scroll</p>
		<div class="scroll-image">
			<img src="<?php echo get_template_directory_uri(); ?>/images/scroll_down.png"/>
		</div>
	</div>
	<?php } ?>

<div id="copyright">
<!--<?php echo sprintf( __( '%1$s %2$s %3$s. All Rights Reserved.', 'blankslate' ), '&copy;', date('Y'), esc_html(get_bloginfo('name')) ); echo sprintf( __( ' Theme By: %1$s.', 'blankslate' ), '<a href="http://tidythemes.com/">TidyThemes</a>' ); ?>-->
</div>
	<!--<div class="timeline">
		<div class="timeline_box">
		</div>
	</div>-->
</footer>
</div>
<?php wp_footer(); ?>
</body>
</html>
