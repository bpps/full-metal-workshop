<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
<meta charset="<?php bloginfo('charset'); ?>" />
<meta name="viewport" content="width=device-width" />
<title><?php wp_title(' | ', true, 'right'); ?></title>
<!-- <link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_uri(); ?>" /> -->
<!-- <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900" rel="stylesheet"> -->
<!-- <link href="https://fonts.googleapis.com/css?family=Oswald:300,400,700" rel="stylesheet"> -->
<link href="https://fonts.googleapis.com/css?family=IBM+Plex+Sans:100,100i,200,400,400i,700" rel="stylesheet">
<link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon.ico" />


<script src="https://use.typekit.net/lhh5yku.js"></script>
<script>try{Typekit.load({ async: true });}catch(e){}</script>
<script>
var turl = '<?php bloginfo("template_directory"); ?>';
var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';
</script>
<?php wp_head(); ?>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-156924392-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-156924392-1');
</script>

</head>
<body <?php body_class(); ?>>

<div id="wrapper" class="hfeed">
	<?php if(is_front_page()){
		$vidRoot = get_template_directory_uri();
		$vidRoot = 'https://fullmetalworkshop.com/wp-content/themes/fullmetalworkshop'; ?>
	<div class="background-video">
		<video id="video-background" muted autoplay loop><source src="<?php echo $vidRoot; ?>/images/full_metal_reel.mp4" type="video/mp4"></video>
		<div class="background-cover"></div>
	</div>
	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
		<div class="homepage-content-holder">
			<div class="homepage-content">
				<?php the_content(); ?>
			</div>
		</div>
	<?php endwhile; endif; ?>
	<?php } ?>
<header id="header" role="banner">
  <div id="logo">
    <?php echo file_get_contents( get_template_directory_uri() . '/images/fmw-logo.svg' ); ?>
  </div>
	<nav id="menu" role="navigation">
		<?php wp_nav_menu( array( 'theme-location' => 'main-menu' ) ); ?>
	</nav>
</header>
<div id="container">
