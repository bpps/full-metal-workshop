<?php get_header(); ?>
<section id="content" role="main">

<?php $projectsLoop = new WP_Query(array( 'post_type' => 'project' , 'posts_per_page' => -1, 'order' => 'DESC' ) );
	if( $projectsLoop->have_posts() ): ?>
		<div class="max-width-content">
			<section class="section post-box post-box-0">
				<div class="video-toggle">
					<div class="toggle-icon-off toggle-icon">
						<img src="<?php echo get_template_directory_uri(); ?>/images/video_off.png"/>
					</div>
					<div class="toggle-icon-on toggle-icon on">
						<img src="<?php echo get_template_directory_uri(); ?>/images/video.png"/>
					</div>
				</div>
			</section>
		  <?php while( $projectsLoop->have_posts() ): $projectsLoop->the_post();
				$postimage = wp_get_attachment_image_src(get_post_thumbnail_id($id), 'large' ); ?>
				<section class="section post-box post-box-<?php echo $projectsLoop->current_post + 1; ?>">
					<div class="post-box-image" style="background-image: url('<?php echo $postimage[0]; ?>');">
					</div>
					<div class="post-box-container">
						<div class="inner-content">
							<div class="post-box-content">
								<div class="post-box-excerpt">
									<?php
									if ( $logo = get_field('logo') ) { ?>
										<div class="post-box-logo">
											<img src="<?php echo $logo['sizes']['medium']; ?>"/>
										</div>
									<?php
									} else { ?>
										<h2><?php the_title(); ?></h2>
									<?php
									}
									if($homeDesc = get_field('homepage_description')) {
										echo $homeDesc;
									}
									if($homeLink = get_field('homepage_link')) { ?>
										<div class="project-link-container">
											<a class="project-link" target="<?php echo $homeLink['target']; ?>" href="<?php echo $homeLink['url']; ?>"><?php echo $homeLink['title']; ?></a>
										</div>
									<?php
									} ?>
									<!--<a class="black_background" href="<?php echo get_the_permalink(); ?>">FIND OUT MORE</a>-->
								</div>
							</div>
						</div>
					</div>
					<div class="background-cover">
					</div>
				</section>
			<?php endwhile; ?>
		</div>
		<?php wp_reset_postdata(); ?>
	<?php endif; ?>
</section>
<?php get_footer(); ?>
