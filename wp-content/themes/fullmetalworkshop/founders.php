<?php
/*
Template Name: Founders
*/

get_header();

$founders_query = new WP_Query(array( 'post_type' => 'founders' , 'posts_per_page' => -1, 'order' => 'DESC' ) );
	if( $founders_query->have_posts() ): ?>
		<div id="founders-container">
		  <?php while( $founders_query->have_posts() ): $founders_query->the_post();
				$founder_image = get_the_post_thumbnail_url(get_the_id(), 'large' ); ?>
				<div class="founder-wrapper">
          <div class="founder-container">
            <div class="founder-background" style="background-image:url(<?php echo $founder_image; ?>);">
            </div>
            <div class="founder-content">
              <h2><?php the_title(); ?></h2>
              <?php
              if ( $title = get_field('founder_title') ) { ?>
                <h3><?php echo $title; ?></h3>
              <?php
              }
              the_content();
              if ( $link = get_field('founder_link') ) { ?>
                <a href="<?php echo $link['url']; ?>" target="<?php echo $link['target']; ?>">
                  <?php echo $link['title']; ?>
                </a>
              <?php
              } ?>
            </div>
          </div>
        </div>
			<?php endwhile; ?>
		</div>
		<?php wp_reset_postdata(); ?>
	<?php endif;
get_footer(); ?>
