<?php
/**
 * Template Name:  Dev Posts
 *
 * The template for displaying the developer blog landing page
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Full Metal Workshop
 */
get_header(); ?>
<section id="content" role="main">
	<div class="inner-content blog-items large">
		<?php
    $dev_post_query = new WP_Query( array( 'post_type' => 'dev-post' ) );
    if ( $dev_post_query->have_posts()) :
      while ( $dev_post_query->have_posts()): $dev_post_query->the_post(); ?>
  			<div class="post-item">
  				<div class="post-item-inner">
  					<?php
  					if ( $terms = get_the_terms( get_the_id(), 'dev-cat' ) ) { ?>
  						<div class="post-cats">
  							<?php
  							foreach ( $terms as $term ) {
  								$color = get_field('category_color', $term) ? get_field('category_color', $term) : 'black'; ?>
  								<div class="post-cat" style="border-color: <?php echo $color; ?>"><span><?php echo $term->name; ?></span></div>
  							<?php
  							} ?>
  						</div>
  					<?php
  					}
  					$image = get_template_directory_uri() . '/images/fmw_logo_big_1.png';
  					if ( has_post_thumbnail() ) {
  						$image = wp_get_attachment_image_url( get_post_thumbnail_id(), 'medium' );
  					} ?>
  					<a href="<?php the_permalink(); ?>" class="post-item-image centered-bg" style="background-image:url(<?php echo $image; ?>);">
  					</a>
  					<div class="post-item-content">
  						<h3 class="post-item-title">
  							<a href="<?php the_permalink(); ?>">
  								<?php the_title(); ?>
  							</a>
  						</h3>
  						<?php
  						if ( has_excerpt() ) { ?>
  							<div class="post-item-excerpt">
  								<?php the_excerpt(); ?>
  							</div>
  						<?php
  						} ?>
  					</div>
  				</div>
  			</div>
  		<?php endwhile;
      wp_reset_postdata();
    endif; ?>
	</div>
</section>
<?php get_footer(); ?>
