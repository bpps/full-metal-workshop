<?php get_header(); ?>
<section id="content" role="main">
	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
		<div class="inner-content">
			<?php if(has_post_thumbnail()){ ?>
				<?php $image = wp_get_attachment_image_url( get_post_thumbnail_id(), 'large' ); ?>
				<div class="header-image-container">
					<div class="header-image-sizer">
					</div>
					<div class="header-image centered-bg" style="background-image:url(<?php echo $image; ?>);">
					</div>
					<div class="header-image-content">
						<h1 class="entry-title"><?php the_title(); ?></h1>
					</div>
				</div>
			<?php }else{ ?>
					<h1 class="entry-title"><?php the_title(); ?></h1>
			<?php } ?>
			<div class="article_content">
				<?php the_content(); ?>
			</div>
			<nav class="post-nav">
				<div>
					<?php
					if ( $previous = get_previous_post() ) { ?>
					  <a class="post-nav-prev" href="<?php echo get_the_permalink($previous); ?>">
							<p><?php echo get_the_title($previous); ?></p>
							<span><?php echo date('F j, Y', strtotime($previous->post_date)); ?></span>
						</a>
					<?php
					} ?>
				</div>
				<div>
					<?php
					if ( $next = get_next_post() ) { ?>
					  <a class="post-nav-next" href="<?php echo get_the_permalink($next); ?>">
							<p><?php echo get_the_title($next); ?></p>
							<span><?php echo date('F j, Y', strtotime($next->post_date)); ?></span>
						</a>
					<?php
					} ?>
				</div>
			</nav>
		</div>
	<?php endwhile; endif; ?>
</section>
<?php get_footer(); ?>
