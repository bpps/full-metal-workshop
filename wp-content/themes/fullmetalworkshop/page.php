<?php get_header(); ?>
<section id="content" role="main">
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
  <div class="inner-content">
    <?php if(has_post_thumbnail()){ ?>
      <?php $image = wp_get_attachment_image_url( get_post_thumbnail_id(), 'large' ); ?>
      <div class="header-image-container">
        <div class="header-image-sizer">
        </div>
        <div class="header-image centered-bg" style="background-image:url(<?php echo $image; ?>);">
        </div>
        <div class="header-image-content">
          <h1 class="entry-title"><?php the_title(); ?></h1>
        </div>
      </div>
    <?php }else{ ?>
        <h1 class="entry-title"><?php the_title(); ?></h1>
    <?php } ?>
    <div class="article_content">
      <?php the_content(); ?>
    </div>
    
  </div>
</article>
<?php if ( ! post_password_required() ) comments_template('', true); ?>
<?php endwhile; endif; ?>
</section>
<?php get_footer(); ?>
