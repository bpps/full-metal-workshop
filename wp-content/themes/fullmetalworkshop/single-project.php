<?php get_header(); ?>
<section id="content" role="main">
<?php if ( have_posts() ) : while ( have_posts() ) : the_post();
	$podcastImage = get_field('podcast_image');
?>
	<div class="inner-content">
		<?php if(has_post_thumbnail()){ ?>
			<?php $image = wp_get_attachment_image_url( get_post_thumbnail_id(), 'large' ); ?>
			<div class="header-image-container">
				<div class="header-image-sizer">
				</div>
				<div class="header-image centered-bg" style="background-image:url(<?php echo $image; ?>);">
				</div>
				<div class="header-image-content">
					<h1 class="entry-title"><?php the_title(); ?></h1>
				</div>
			</div>
		<?php }else{ ?>
				<h1 class="entry-title"><?php the_title(); ?></h1>
		<?php } ?>
		<div class="article-content">
			<?php the_content(); ?>
		</div>
		<?php if( have_rows('episodes') ): ?>
			<div class="episodes">
				<?php while ( have_rows('episodes') ) : the_row(); ?>
					<div class="episode">
						<div class="episode-image">
							<?php
							if(get_sub_field('image')){
								$pcImage = get_sub_field('image');
								$pcImage = $pcImage['sizes']['small'];
							}else{
								$image = $podcastImage['sizes']['small'];
							} ?>
							<img src="<?php echo $image; ?>"/>
						</div>
						<div class="episode-info">
							<h3><?php echo get_sub_field('episode') ? get_sub_field('episode').'. ' : ''; ?><?php echo get_sub_field('title'); ?></h3>
							<?php if(get_sub_field('description')){ ?>
								<div class="episode-desc">
									<?php echo get_sub_field('description'); ?>
								</div>
							<?php } ?>
							<?php if( have_rows('links') ): ?>
								<?php while( have_rows('links')): the_row(); ?>
									<a target="_blank" href="<?php echo get_sub_field('url'); ?>"><?php echo get_sub_field('title'); ?></a>
								<?php endwhile; ?>
							<?php endif; ?>
						</div>
					</div>
				<?php endwhile; ?>
			</div>
		<?php endif; ?>
	</div>
<?php endwhile; endif; ?>
<footer class="footer">
</footer>
</section>
<?php get_footer(); ?>
