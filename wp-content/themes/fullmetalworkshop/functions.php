<?php
add_action('after_setup_theme', 'blankslate_setup');
function blankslate_setup()
{
load_theme_textdomain('blankslate', get_template_directory() . '/languages');
add_theme_support('automatic-feed-links');
// show_admin_bar( true );

add_image_size( 'small', 200 );
add_image_size( 'medium', 500 );
add_image_size( 'large', 1400 );
add_image_size( 'full', 1900 );

add_theme_support('post-thumbnails');
global $content_width;
if ( ! isset( $content_width ) ) $content_width = 640;
register_nav_menus(
array( 'main-menu' => __( 'Main Menu', 'blankslate' ) )
);
}

add_action( 'wp_enqueue_scripts', 'blankslate_load_scripts' );
function blankslate_load_scripts()
{
	wp_enqueue_script( 'jquery' );

	wp_register_script( 'jquery-easing', "https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js");
	//wp_enqueue_script("jquery-easing");

	wp_register_script( 'fullpage-overflow', get_template_directory_uri()."/js/fullpage/vendors/scrolloverflow.min.js");
	wp_enqueue_script("fullpage-overflow");

	wp_register_script( 'fullpage', get_template_directory_uri()."/js/fullpage/dist/fullpage.min.js");
	wp_enqueue_script("fullpage");

	wp_register_script( 'masonry', get_template_directory_uri()."/js/masonry.pkgd.min.js");
	//wp_enqueue_script("masonry");

	wp_register_script( 'isotope', get_template_directory_uri()."/js/isotope.pkgd.min.js");
	//wp_enqueue_script("isotope");

	wp_register_script( 'packery', get_template_directory_uri()."/js/packery-mode.pkgd.min.js");
	//wp_enqueue_script("packery");

	wp_register_script( 'imagesloaded', get_template_directory_uri()."/js/imagesloaded.pkgd.min.js");
	wp_enqueue_script("imagesloaded");

	wp_register_script( 'cookie', get_template_directory_uri()."/js/js.cookie.js");
	//wp_enqueue_script("cookie");

	wp_register_script( 'slick', get_template_directory_uri()."/js/slick/slick.js");
	//wp_enqueue_script("slick");

	wp_enqueue_style( 'jquery-ui-style',"https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css");

	wp_register_script( 'jquery-ui', "https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js");
	//wp_enqueue_script("jquery-ui");

	//wp_enqueue_style( 'slick-style',"//cdn.jsdelivr.net/jquery.slick/1.5.9/slick.css");

	wp_enqueue_style('https://fonts.googleapis.com/css?family=Open+Sans:400,700,600,300');

	wp_enqueue_style('http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,300italic,700');

	$manifest = json_decode(file_get_contents('dist/assets.json', true));
	$main = $manifest->main;

	wp_enqueue_style( 'fmw-style', get_template_directory_uri() . $main->css, false, null );

	wp_enqueue_script('fmw-js', get_template_directory_uri() . $main->js, ['jquery'], null, true);

}

add_action('wp_head','pluginname_ajaxurl');
function pluginname_ajaxurl() { ?>
	<script type="text/javascript">
		var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';
		var templateurl = '<?php echo get_template_directory_uri(); ?>';
		var homeurl = '<?php echo get_home_url(); ?>';
		var siteurl = '<?php echo site_url(); ?>';
	</script>
<?php
}

//add_action('comment_form_before', 'blankslate_enqueue_comment_reply_script');
function blankslate_enqueue_comment_reply_script()
{
if (get_option('thread_comments')) { wp_enqueue_script('comment-reply'); }
}
add_filter('the_title', 'blankslate_title');
function blankslate_title($title) {
if ($title == '') {
return '&rarr;';
} else {
return $title;
}
}
add_filter('wp_title', 'blankslate_filter_wp_title');
function blankslate_filter_wp_title($title)
{
return $title . esc_attr(get_bloginfo('name'));
}
add_action('widgets_init', 'blankslate_widgets_init');
function blankslate_widgets_init()
{
register_sidebar( array (
'name' => __('Sidebar Widget Area', 'blankslate'),
'id' => 'primary-widget-area',
'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
'after_widget' => "</li>",
'before_title' => '<h3 class="widget-title">',
'after_title' => '</h3>',
) );
}

function custom_taxonomies(){
	$labels = array(
		'name'                       => _x( 'Project Types', 'taxonomy general name', 'fmw' ),
		'singular_name'              => _x( 'Project Type', 'taxonomy singular name', 'fmw' ),
		'search_items'               => __( 'Search Project Types', 'fmw' ),
		'popular_items'              => __( 'Popular Project Types', 'fmw' ),
		'all_items'                  => __( 'All Project Types', 'fmw' ),
		'parent_item'                => null,
		'parent_item_colon'          => null,
		'edit_item'                  => __( 'Edit Project Type', 'fmw' ),
		'update_item'                => __( 'Update Project Type', 'fmw' ),
		'add_new_item'               => __( 'Add New Project Type', 'fmw' ),
		'new_item_name'              => __( 'New Project Type Name', 'fmw' ),
		'separate_items_with_commas' => __( 'Separate project types with commas', 'fmw' ),
		'add_or_remove_items'        => __( 'Add or remove project types', 'fmw' ),
		'choose_from_most_used'      => __( 'Choose from the most used project types', 'fmw' ),
		'not_found'                  => __( 'No project types found.', 'fmw' ),
		'menu_name'                  => __( 'Project Types', 'fmw' ),
	);

	$args = array(
    'show_tagcloud'         => true,
		'hierarchical'          => true,
		'labels'                => $labels,
		'show_ui'               => true,
		'show_admin_column'     => true,
		'show_in_nav_menus'     => true,
		'public'								=> true,
		'update_count_callback' => '_update_post_term_count',
		'query_var'             => true,
		'rewrite'               => true
	);

	register_taxonomy( 'project-type', array('post'), $args );
	register_taxonomy_for_object_type( 'project-type', array('post') );

	$labels = array(
		'name'                       => _x( 'Dev Post Types', 'taxonomy general name', 'fmw' ),
		'singular_name'              => _x( 'Dev Post Type', 'taxonomy singular name', 'fmw' ),
		'search_items'               => __( 'Search Dev Post Types', 'fmw' ),
		'popular_items'              => __( 'Popular Dev Post Types', 'fmw' ),
		'all_items'                  => __( 'All Dev Post Types', 'fmw' ),
		'parent_item'                => __( 'Parent Dev Post Type', 'fmw' ),
		'parent_item_colon'          => __( 'Parent Dev Post Type:', 'fmw' ),
		'edit_item'                  => __( 'Edit Dev Post Type', 'fmw' ),
		'update_item'                => __( 'Update Dev Post Type', 'fmw' ),
		'add_new_item'               => __( 'Add New Dev Post Type', 'fmw' ),
		'new_item_name'              => __( 'New Dev Post Type Name', 'fmw' ),
		'separate_items_with_commas' => __( 'Separate dev post types with commas', 'fmw' ),
		'add_or_remove_items'        => __( 'Add or remove dev post types', 'fmw' ),
		'choose_from_most_used'      => __( 'Choose from the most used dev post types', 'fmw' ),
		'not_found'                  => __( 'No dev post types found.', 'fmw' ),
		'menu_name'                  => __( 'Dev Post Types', 'fmw' ),
	);

	$args = array(
    'show_tagcloud'         => true,
		'hierarchical'          => false,
		'labels'                => $labels,
		'show_ui'               => true,
		'show_admin_column'     => true,
		'show_in_nav_menus'     => true,
		'show_in_rest' 					=> true,
		'public'								=> true,
		'update_count_callback' => '_update_post_term_count',
		'query_var'             => true,
		'rewrite'               => true
	);

	register_taxonomy( 'dev-cat', array('dev-post'), $args );
	register_taxonomy_for_object_type( 'dev-cat', array('dev-post') );

}

add_action( 'init', 'custom_taxonomies');

function add_custom_post_types() {

	$labels = array(
		'name' => _x('Projects', 'post type general name'),
		'singular_name' => _x('Project', 'post type singular name'),
		'add_new' => _x('Add New', 'Project'),
		'add_new_item' => __('Add New Project'),
		'edit_item' => __('Edit Project'),
		'new_item' => __('New Project'),
		'view_item' => __('View Project'),
		'search_items' => __('Search Projects'),
		'not_found' =>  __('Nothing found'),
		'not_found_in_trash' => __('Nothing found in Trash'),
		'parent_item_colon' => ''
	);

	$args = array(
		'labels' => $labels,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'query_var' => true,
		'menu_icon' => get_template_directory_uri() . '/images/icons/projects.png',
		'rewrite' => true,
		'capability_type' => 'post',
		'hierarchical' => false,
		'menu_position' => null,
		'supports' => array('title','editor','thumbnail'),
		'taxonomies' => array('category'),
	  );

	register_post_type( 'project' , $args );

	$labels = array(
		'name' => _x('Dev Posts', 'post type general name'),
		'singular_name' => _x('Dev Post', 'post type singular name'),
		'add_new' => _x('Add New', 'Dev Post'),
		'add_new_item' => __('Add New Dev Post'),
		'edit_item' => __('Edit Dev Post'),
		'new_item' => __('New Dev Post'),
		'view_item' => __('View Dev Post'),
		'search_items' => __('Search Dev Posts'),
		'not_found' =>  __('Nothing found'),
		'not_found_in_trash' => __('Nothing found in Trash'),
		'parent_item_colon' => ''
	);

	$args = array(
		'labels' => $labels,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'query_var' => true,
		'rewrite' => array('slug' => 'developer-blog'),
		'has_archive' => false,
		'capability_type' => 'post',
		'hierarchical' => true,
		'menu_position' => null,
		'show_in_rest' => true,
		'supports' => array('title','editor','thumbnail', 'excerpt', 'page-attributes'),
		'taxonomies' => array('dev-cat'),
	  );

	register_post_type( 'dev-post' , $args );

	$labels = array(
		'name' => _x('Founders', 'post type general name'),
		'singular_name' => _x('Founder', 'post type singular name'),
		'add_new' => _x('Add New', 'Founder'),
		'add_new_item' => __('Add New Founder'),
		'edit_item' => __('Edit Founder'),
		'new_item' => __('New Founder'),
		'view_item' => __('View Founder'),
		'search_items' => __('Search Founders'),
		'not_found' =>  __('Nothing found'),
		'not_found_in_trash' => __('Nothing found in Trash'),
		'parent_item_colon' => ''
	);

	$args = array(
		'labels' => $labels,
		'public' => false,
		'publicly_queryable' => true,
		'show_ui' => true,
		'query_var' => true,
		'menu_icon' => get_template_directory_uri() . '/images/icons/projects.png',
		'rewrite' => true,
		'capability_type' => 'post',
		'hierarchical' => false,
		'menu_position' => null,
		'supports' => array('title','editor','thumbnail'),
		'taxonomies' => [],
	  );

	register_post_type( 'founders' , $args );
}

add_action('init', 'add_custom_post_types');

add_action('wp_ajax_nopriv_do_ajax', 'ajax_function');
add_action('wp_ajax_do_ajax', 'ajax_function');

function ajax_function(){
     switch($_REQUEST['function']){
         case 'getfooter':
         	   $output = getfooter();
               echo $output;
         break;
     }
     die;
}

function getfooter(){
	$count_posts = wp_count_posts();
	$published_posts = $count_posts->publish;
	$postCount = 0;
	$postArray = array();
	$my_secondary_loop = new WP_Query(array( 'post_type' => 'post' , 'posts_per_page' => -1, 'order' => 'ASC' ) );
			if( $my_secondary_loop->have_posts() ):
			    while( $my_secondary_loop->have_posts() ): $my_secondary_loop->the_post();
					 $postType = get_post_type();
						$postObj = new stdClass();
						$postObj->name = get_the_title();
						$postObj->date = get_the_date('U');
						$postObj->excerpt = htmlentities(get_the_excerpt());
						$postObj->imageUrl = $thumb['0'];
						$postObj->url = get_the_permalink();
						$postObj->type = get_post_type();
						array_push($postArray, $postObj);
						$postCount ++;
						endwhile;
						endif;
						wp_reset_postdata();
						return json_encode($postArray);
}

function my_scripts_method() {
        wp_enqueue_script(
            'easing',
            get_template_directory_uri() . '/js/jquery.easing.1.3.js',
            array('jquery')
        );
    }
    add_action('wp_enqueue_scripts', 'my_scripts_method');

function blankslate_custom_pings($comment)
{
$GLOBALS['comment'] = $comment;
?>
<li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>"><?php echo comment_author_link(); ?></li>
<?php
}
add_filter('get_comments_number', 'blankslate_comments_number');
function blankslate_comments_number($count)
{
if (!is_admin()) {
global $id;
$comments_by_type = &separate_comments( get_comments( 'status=approve&post_id=' . $id ) );
return count($comments_by_type['comment']);
} else {
return $count;
}
}
