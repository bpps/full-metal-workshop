<?php get_header(); ?>
<section id="content" role="main">
	<div class="inner-content blog-items large">
		<?php
		if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
			<div class="post-item">
				<div class="post-item-inner">
					<?php
					$image = get_template_directory_uri() . '/images/fmw_logo_big_1.png';
					if ( has_post_thumbnail() ) {
						$image = wp_get_attachment_image_url( get_post_thumbnail_id(), 'medium' );
					} ?>
					<a href="<?php the_permalink(); ?>" class="post-item-image centered-bg" style="background-image:url(<?php echo $image; ?>);">
					</a>
					<div class="post-item-content">
						<h3 class="post-item-title">
							<a href="<?php the_permalink(); ?>">
								<?php the_title(); ?>
							</a>
						</h3>
						<?php
						if ( has_excerpt() ) { ?>
							<div class="post-item-excerpt">
								<?php the_excerpt(); ?>
							</div>
						<?php
						} ?>
					</div>
				</div>
			</div>
		<?php endwhile; endif; ?>
	</div>
</section>
<?php get_footer(); ?>
