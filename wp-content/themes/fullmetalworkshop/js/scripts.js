import $ from 'jquery'

var bgvideo;
var screenWidth;
var markerWidth = 18;
var timelineWidth;
var timelineArray = [];

$('#toggle-menu').on('click', function(){
  console.log('click')
  if ($('body').hasClass('menu-inactive') || $('body').hasClass('menu-active')) {
    $('body').toggleClass('menu-inactive');
    $('body').toggleClass('menu-active');
  }else {
    $('body').addClass('menu-active');
  }
});

if($('.max-width-content').length) {
  new fullpage('.max-width-content', {
    licenseKey: 'BC0F3CD2-F6B7492F-823431C0-2FC2FC21',
    sectionSelector: '.post-box',
    scrollOverflow: true,
    onLeave: function(origin, destination, direction){
      if(destination.index == 0) { // home screen
        bgvideo.play();
      } else {
        bgvideo.pause();
      }
      $(destination.item).addClass('from-'+direction);
      setTimeout(function() {
        $(destination.item).removeClass('from-'+direction);
      }, 10);
      theyScrolled = true;
			if($('.scroll-down').is(':visible')){
				$('.scroll-down').fadeOut();
			}
    },
    afterLoad: function(origin, destination, direction){

    },
  });
}

if($('.homepage-content-holder').length > 0){
	setTimeout(function(){
		$('.homepage-content-holder').animate({'opacity':1}, 1000)
	}, 1000);

	setTimeout(function(){
		if(!theyScrolled){
			$('.scroll-down').fadeIn();
		}
	}, 5000);
	//scrollCheck();
}

if($('#video-background').length) {
  bgvideo = document.getElementById('video-background');
  // bgvideo.addEventListener('play', function(e) {
  //   $('.max-width-content').addClass('active');
  // }, true);
  bgvideo.addEventListener('load', function(e) {
    bgvideo.play();
  }, true);
	//$('#video-background').prop('muted', false);
	//bgvideo.volume = 0.0;
}

$('.video-toggle').click(function(){
	if ($('.background-cover').is(':visible')){
		$('.toggle-icon').removeClass('on');
		$('.toggle-icon-off').addClass('on');
		$('.background-cover, .inner-content, nav#menu').fadeOut();
		//bgvideo.volume = 1.0;
		// for (var i=0;i<=10;i++) {
		// 	(function(ind) {
		// 		setTimeout(function(){
		// 			bgvideo.volume = ind/10;
		// 		}, 150 * ind);
   	// 		})(i);
		// }
    $('#video-background').prop('muted', false);
		$('.homepage-content-holder').fadeOut();
	}else{
		$('.toggle-icon').removeClass('on');
		$('.toggle-icon-on').addClass('on');
		$('.background-cover, .inner-content').fadeIn();
		// for (var i=10; i>=0; i--) {
		// 	(function(ind) {
		// 		setTimeout(function(){
		// 			console.log(ind);
		// 			bgvideo.volume = 1- (ind/10);
		// 		}, 150 * ind);
   	// 		})(i);
		// }
    $('#video-background').prop('muted', true);
		$('.homepage-content-holder').fadeIn();
	}
});

$( window ).resize(function() {

});

var theyScrolled = false;
$(window).scroll(function(){
	//scrollCheck();
	theyScrolled = true;
	if($('.scroll-down').is(':visible')){
		$('.scroll-down').fadeOut();
	}
});

function scrollCheck(){
	if($('body').hasClass('home')){
	var winTop = $(window).scrollTop();
	var winHeight = $(window).height();
	var winDif = 1/(winHeight/4);
	$('.post-box').each(function(i, item){
		var pbTop = $(item).position();
			pbTop = pbTop.top;
		if(winTop > pbTop - winHeight / 4 && winTop <= pbTop){
			$('.post-box-container').css('opacity', 0);
			$('.post-box-container', item).css('opacity',  1 - (((pbTop - (winTop - winHeight/4)) * winDif)-1));
		}else if(winTop > pbTop + (winHeight / 6) && winTop < (pbTop + winHeight)){
			$('.post-box-container', item).css('opacity',  1-(1-((pbTop - (winTop - winHeight/6)) * winDif)-1));
			console.log(1-(1-((pbTop - (winTop - winHeight/6)) * winDif)-1));
		}else if(winTop < winHeight - (winHeight/4)){
			$('.post-box-container').css('opacity', 0);
		}else if(winTop > (winHeight / 4) * 3){
			if (!$('.background-cover').is(':visible')){
				$('.toggle-icon').removeClass('on');
				$('.toggle-icon-on').addClass('on');
				$('.background-cover, .inner-content, nav#menu').fadeIn();
				for (var i=10; i>=0; i--) {
					(function(ind) {
						setTimeout(function(){
							bgvideo.volume = 1- (ind/10);
						}, 150 * ind);
   					})(i);
				}
				$('.homepage-content-holder').fadeIn();
			}
		}
	});
	}
}
